FROM bitnami/minideb:bullseye
 
LABEL author="Aurélien COTTIN"
LABEL description="BIOCOM-PIPE 1.20 image"
LABEL maintainer="aurelien.cottin@inrae.fr"

# Install libraries
RUN apt-get update && apt-get install -y --no-install-recommends \
build-essential   \
autoconf          \
ca-certificates   \
curl              \
cpanminus         \
git               \
infernal          \
libfreetype6-dev  \
libgmp3-dev       \
make              \
pkg-config        \
python2.7-minimal \
python-dev        \
python-setuptools \
&& rm -rf /var/lib/apt/lists/*


# Install perl libraries using cpanminus
RUN cpanm                \
Config::AutoConf         \
File::ShareDir           \
File::ShareDir::Install  \
Inline                   \
Inline::C                \
Math::Int64              \
Module::CAPIMaker        \
Parse::RecDescent        \
Pegex                    \
Text::Template

# Install pip for python 2
RUN curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o get-pip.py \
&& python get-pip.py

# Install python2 libraries using pip and a requirement file to install them sequentially
COPY python2_requirements.txt ./
RUN cat python2_requirements.txt | xargs -n 1 -L 1 pip install --no-cache-dir

# Install other tools
WORKDIR /opt

# Install easel using git
RUN git clone https://github.com/EddyRivasLab/easel \
&& cd easel                                         \
&& autoconf                                         \
&& ./configure                                      \
&& make                                             \
&& make install                                     \
&& rm -rf /opt/easel 

# Install biocom-pipe using git
RUN git clone https://forgemia.inra.fr/biocom/biocom-pipe

# Get biocom-pipe databases (v 1.20)
WORKDIR /opt/biocom-pipe

RUN curl -SL https://zenodo.org/record/4428756/files/archive_BIOCOM-PIPE-V1.20.tgz -o archive.tgz \
&& tar -xvzf archive.tgz                                                                          \
&& rm archive.tgz

ENV PATH="/opt/biocom-pipe:${PATH}"

## FOR DEBUG
#RUN apt-get update && apt-get install -y --no-install-recommends less vim \
#&& echo "alias ll='ls -alh --color=auto'" >> /root/.bashrc            

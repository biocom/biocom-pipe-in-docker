# BIOCOM-PIPE in docker

BIOCOM-PIPE pipeline in a docker with everything needed to run it.

- code is here: [https://forgemia.inra.fr/biocom/biocom-pipe](https://forgemia.inra.fr/biocom/biocom-pipe)
- publication: [BIOCOM-PIPE: a new user-friendly metabarcoding pipeline for the characterization of microbial diversity from 16S, 18S and 23S rRNA gene amplicons](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-020-03829-3)
- See zenodo repository for code and datasets in other format : [BIOCOM-PIPE in zenodo](https://zenodo.org/search?page=1&size=20&q=biocom%20pipe)

*!* The build automatically download all the databases used - **20Go uncompressed** *!*

# Details

Some explaination about pieces and choices in the `Dockerfile`

## package list and why they are needed

packages | why it is needed
 -- | --
`autoconf` | to build stuff (like `easel`, downloaded from github)
`build-essential` | to build stuff (like `perl` package `YAML::XS` that need `stdlib.h` or simply `gcc`)
`ca-certificates` | to avoid error msg from `curl`
`curl` | to download stuff
`cpanminus` | lightweight `CPAN` client (and zeroconf)
`git` | to download and build stuff (like `easel`)
`infernal` | aligner used by `BIOCOM-PIPE`
`libfreetype6-dev` | for `python` package `matplotlib==1.4.3`
`libgmp3-dev` | for `python` package `gmpy`
`make` | to build stuff
`pkg-config` | to build stuff (like `python` package `matplotlib==1.4.3`)
`python2.7-minimal` | to run the `python` part of the pipeline
`python-dev` | to build some `python` packages (like `gmpy` that need `python.h`)
*`python-pip`* | *to install `python` packages*
`python-setuptools` | install helper for some package (`cogent`)

## PIP trick

- pip2 is no longer in repo, it can be installed using pypa.io (https://stackoverflow.com/a/64240018)
- to install `pip` packages one by one (as `cogent` wont install without `numpy` and old `pip` dont work sequentially), the trick used with `cat` / `xargs` is based on answer from https://stackoverflow.com/questions/5394356/how-to-specify-install-order-for-python-pip

## Container size

- usage of `minideb` instead of `debian` for smaller container size (https://dzone.com/articles/minideb-a-minimalist-debian-based-docker-image)

## Infernal

- `Infernal` is available as `debian` package but do not include `easel`, that is needed for `esl-alimerge` command. 
- Choice is to install `infernal` with `apt` and `easel` with `git`.

